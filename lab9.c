#include <stdio.h>
int main()
{
   int first, second, *p, *q, sum,difference,product,remainder;
   float quotient;

   printf("Enter two integers to perform arithmetic operations\n");
   scanf("%d%d", &first, &second);

   p = &first;
   q = &second;

   sum = *p + *q;
   difference=*p - *q;
   product=(*p)*(*q);
   quotient=(*p)/(*q);
   remainder=(*p)%(*q);

   printf("Sum of the numbers = %d\n", sum);
   printf("difference of numbers = %d\n",difference);
   printf("product of numbers = %d\n",product);
   printf("quotient of the numbers = %f\n",quotient);
   printf("remainder of the numbers = %d\n",remainder);

   return 0;
}
